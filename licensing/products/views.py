from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.core.exceptions import PermissionDenied
from django.contrib.auth.decorators import login_required
from django.urls import reverse

from .models import Product, InstallationFile
from .forms import ProductForm, InstallationFileForm
from profiles.models import Company


companies = Company.objects.all()


@login_required(login_url='/')
def get_products(request):
    products = Product.objects\
                      .filter(company_id=request.session['company_id'])\
                      .values('id', 'product_name', 'short_code')\
                      .order_by('product_name')
    form = ProductForm(request.POST or None)

    if form.is_valid():
        new_product = form.save(commit=False)
        new_product.company = Company.objects.get(id=request.session['company_id'])
        new_product.save()
        return HttpResponseRedirect(reverse('products'))

    return render(request, 'products/product_list.html', {'products': products,
                                                          'form': form,
                                                          'companies': companies,
                                                          'selected_company': request.session['company_id']})


@login_required(login_url='/')
def get_installation_files(request, product_id):
    product = get_object_or_404(Product.objects.prefetch_related('installation_files'), id=product_id)
    if product.company_id != request.session['company_id']:
        raise PermissionDenied
    installation_files = product.installation_files.all()
    form = InstallationFileForm(request.POST or None)

    if form.is_valid():
        new_installation_file = form.save(commit=False)
        new_installation_file.product = product
        new_installation_file.save()
        return HttpResponseRedirect(reverse('installation_files', args=(product_id,)))

    return render(request, 'installation_files/installation_file_list.html', {'installation_files': installation_files,
                                                                              'product': product,
                                                                              'companies': companies,
                                                                              'selected_company': request.session['company_id'],
                                                                              'form': form})


@login_required(login_url='/')
def edit_installation_file(request, product_id, installation_file_id):
    product = get_object_or_404(Product, id=product_id)
    if product.company_id != request.session['company_id']:
        raise PermissionDenied
    installation_file = get_object_or_404(InstallationFile, id=installation_file_id)
    form = InstallationFileForm(request.POST or None, instance=installation_file)

    if form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse('installation_files', args=(product_id,)))

    return render(request, 'installation_files/installation_file_update.html', {'installation_file': installation_file,
                                                                                'product_id': product_id,
                                                                                'companies': companies,
                                                                                'selected_company': request.session['company_id'],
                                                                                'form': form})
