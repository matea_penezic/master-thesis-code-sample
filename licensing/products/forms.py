from datetime import date
from django import forms
from django.utils.translation import ugettext_lazy as _

from .models import Product, InstallationFile


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['product_name', 'short_code']
        error_messages = {
            'product_name': {
                'unique': _('Product name not available'),
            },
            'short_code': {
                'unique': _('Short code not available'),
            },
        }

    def clean(self):
        cleaned_data = super(ProductForm, self).clean()

        if len(cleaned_data['short_code']) != 2:
            raise forms.ValidationError('Short code must be exactly 2 characters long.')


class InstallationFileForm(forms.ModelForm):
    release_date = forms.CharField(label='Release date', initial=date.today)

    class Meta:
        model = InstallationFile
        fields = ['version', 'full_link', 'filename', 'release_date', 'enabled']
        labels = {
            'hash_md5': 'File hash'
        }


