from django.db import models

from profiles.models import Company


class Product(models.Model):
    product_name = models.CharField(unique=True, max_length=100, null=True)
    short_code = models.CharField(unique=True, max_length=2, null=True)
    company = models.ForeignKey(Company)

    def __str__(self):
        return self.product_name


class InstallationFile(models.Model):
    product = models.ForeignKey(Product, related_name='installation_files')
    version = models.CharField(max_length=20, null=True)
    full_link = models.URLField(max_length=1024, null=True)
    filename = models.CharField(max_length=255, blank=True, null=True)
    release_date = models.DateField(blank=True, null=True)
    enabled = models.BooleanField(default=True)

    def __str__(self):
        return self.version
