from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^(?P<product_id>[0-9]+)$', views.get_installation_files, name="installation_files"),
    url(r'^(?P<product_id>[0-9]+)/edit/(?P<installation_file_id>[0-9]+)$', views.edit_installation_file,
        name="edit_installation_file"),
    url(r'^$', views.get_products, name="products"),
]
