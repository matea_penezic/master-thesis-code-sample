from django.contrib import admin

from .models import Product, InstallationFile

admin.site.register(Product)
admin.site.register(InstallationFile)
